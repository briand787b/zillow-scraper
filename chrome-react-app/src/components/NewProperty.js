import React from 'react';
import { Formik } from 'formik';

const NewProperty = () => {
    return (
      <div>
        <Formik
          initialValues={{ firstName: "", lastName: "" }}
          onSubmit={(data, { setSubmitting }) => {
            setSubmitting(true);
            // make async call
            console.log(data);
            setSubmitting(false);
          }}
        >
          {({ values, isSubmitting}) => (
            <Form>
              <Field
                placeholder="first name"
                name="firstName"
                type="input"
                as={TextField}
              />
              <Field
                placeholder="last name"
                name="lastName"
                type="input"
                as={TextField}
              />
              <div>
                <Button disabled={isSubmitting} type="submit">
                  submit
                </Button>
              </div>
              <pre>{JSON.stringify(values, null, 2)}</pre>
            </Form>
          )}
        </Formik>
      </div>
    );
  };

export default NewProperty;