import React from 'react';
import '../styles/PropertyBoxGeneral.css';

const PropertyBoxGeneral = (props) => {
    props = {
        tags: [
            "Mountain View",
            "Big Lot",
            "Log Cabin",
            "Lakefront",
            "Riverfront",
            "National Forest",
            "Pool",
            "Cabin",
        ],
        current_price: (Math.floor(Math.random() * 25000)) + 25000,
    };
    return (
        <div className="property-box-general">
            <div className="general-info">
                <p>Price:</p> <p>{props.current_price}</p>
            </div>
            <div className="tags">
                {props.tags.slice(0, Math.floor(Math.random() * props.tags.length)).map(tag => {
                    return <p className="tag">{tag}</p>
                })}
            </div>
        </div>
    )
};

export default PropertyBoxGeneral;