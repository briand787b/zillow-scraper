package controller

import (
	"net/http"
)

// TagRequest is a request object for the Tag resource
type TagRequest struct {
	Tag string `json:"tag"`
}

// Bind does processing on the TagRequest after it gets decoded
func (m *TagRequest) Bind(r *http.Request) error {
	return nil
}

// TagResponse represents the response object for Tag requests
type TagResponse struct {
	Tag string `json:"tag"`
}

// NewTagResponse creates a new TagResponse
func NewTagResponse(tag string) *TagResponse {
	return &TagResponse{
		Tag: tag,
	}
}

// Render processes a TagResponse before rendering in HTTP response
func (m *TagResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// TagResponseList represents a list of Tag
type TagResponseList struct {
	Tags []string `json:"tags"`
}

// NewTagResponseList converts a slice of tag into a TagResponseList
func NewTagResponseList(tags []string) *TagResponseList {
	return &TagResponseList{
		Tags: tags,
	}
}

// Render does any processing ahead of the go-chi library's rendering
func (l *TagResponseList) Render(w http.ResponseWriter, r *http.Request) error {
	if l.Tags == nil {
		l.Tags = []string{}
	}

	return nil
}
