package controller

import (
	"net/http"
	"zcrapr/core/model"
	"zcrapr/core/perr"
	"zcrapr/core/plog"

	"github.com/go-chi/render"
)

// TagController controls request for Tags
type TagController struct {
	l  plog.Logger
	ts model.TagStore
}

// HandleGetAll handles requests to get all the properties
func (c *TagController) HandleGetAll(w http.ResponseWriter, r *http.Request) {
	keyword := r.URL.Query().Get("keyword")
	take := r.Context().Value(takeCtxKey).(int)

	ctx := r.Context()
	tags, err := model.GetAllTags(ctx, c.l, keyword, take, c.ts)
	if err != nil {
		render.Render(w, r, perr.NewHTTPErrorFromError(ctx, err, "could not get all tags", c.l))
		return
	}

	render.Render(w, r, NewTagResponseList(tags))
}
