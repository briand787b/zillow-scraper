package model

import (
	"context"

	"zcrapr/core/perr"
	"zcrapr/core/plog"

	"github.com/pkg/errors"
)

// GetAllTags returns all Tags (up to take) in the database
func GetAllTags(ctx context.Context, l plog.Logger, keyword string, take int, ts TagStore) ([]string, error) {
	if take < 0 {
		return nil, perr.NewErrInvalid("take must be non-negative number")
	}

	tags, err := ts.GetAllTags(ctx, keyword, take)
	if err != nil {
		return nil, errors.Wrap(err, "could not get all tags from TagStore")
	}

	return tags, nil
}
