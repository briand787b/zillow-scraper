package model

import "context"

type TagStore interface {
	GetAllTags(ctx context.Context, keyword string, take int) ([]string, error)
	GetPropertyIDsByTags(ctx context.Context, tags ...string) ([]string, error)
	GetTagsByPropertyID(ctx context.Context, propertyID string) ([]string, error)
	SetTags(ctx context.Context, propertyID string, tags ...string) error
}
