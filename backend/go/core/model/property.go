package model

import (
	"context"
	"fmt"
	"sync"

	"zcrapr/core/perr"
	"zcrapr/core/plog"

	"github.com/pkg/errors"
)

// Property represents a Property
type Property struct {
	ID         string
	URL        string
	Acreage    float64
	SquareFeet int
	Address    string // TODO: make this more granular in model, but keep as string in Redis
	Tags       []string

	captures []Capture
}

// SearchProperties searches properties.  If neither an address nor a tag(s) is provided, then it will return all properties of up to `take` quantity
func SearchProperties(ctx context.Context, l plog.Logger, address string, tags []string, take int, ps PropertyStore, ts TagStore) ([]Property, error) {
	if take < 0 {
		return nil, perr.NewErrInvalid("take must not be negative number")
	}

	if take < 1 {
		take = 100
	}

	var propertyIDs []string
	var accumulatedErrors error
	var mx sync.RWMutex
	if address == "" && (tags == nil || len(tags) == 0) {
		l.Info(ctx, "getting all property IDs")
		propertyIDs, accumulatedErrors = ps.GetAllPropertyIDs(ctx, take)
	} else {
		var wg sync.WaitGroup

		wg.Add(1)
		go func() {
			defer wg.Done()
			pIDs, err := getPropertyIDsByAddress(ctx, l, address, take, ps)
			if err != nil {
				l.Error(ctx, "failed to get property IDs by address", "error", err.Error())
				mx.Lock()
				defer mx.Unlock()
				accumulatedErrors = errors.Wrap(err, "could not get property IDs by address")
				return
			}
			mx.Lock()
			defer mx.Unlock()
			propertyIDs = append(propertyIDs, pIDs...)
		}()

		wg.Add(1)
		go func() {
			defer wg.Done()
			pIDs, err := getPropertyIDsByTags(ctx, l, tags, ts)
			if err != nil {
				mx.Lock()
				defer mx.Unlock()
				accumulatedErrors = errors.Wrap(err, "could not get property IDs by tag string")
				return
			}
			mx.Lock()
			defer mx.Unlock()
			propertyIDs = append(propertyIDs, pIDs...)
		}()

		wg.Wait()
	}

	if accumulatedErrors != nil {
		return nil, errors.Wrap(accumulatedErrors, "could not get property IDs")
	}

	properties := make([]Property, len(propertyIDs))
	for i, propID := range deduplicatePropertyIDs(propertyIDs) {
		prop, err := GetPropertyByID(ctx, l, propID, ps, ts)
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("could not get property by ID (%s)", propID))
		}

		properties[i] = *prop
	}

	return properties, nil
}

// GetPropertyByID gets a Property by its ID
func GetPropertyByID(ctx context.Context, l plog.Logger, id string, ps PropertyStore, ts TagStore) (*Property, error) {
	if id == "" {
		return nil, perr.NewErrInvalid("cannot search with an empty ID")
	}

	p, err := ps.GetPropertyByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "could not get property by ID from store")
	}

	tags, err := ts.GetTagsByPropertyID(ctx, p.ID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get tags")
	}

	p.Tags = tags

	return p, nil
}

// // GetAllProperties retrieves all Properties
// func GetAllProperties(ctx context.Context, l plog.Logger, take int, ps PropertyStore) ([]Property, error) {
// 	// if skip < 0 {
// 	// 	return nil, perr.NewErrInvalid("skip cannot be negative")
// 	// }

// 	if take < 1 {
// 		take = math.MaxInt32
// 	}

// 	propIDs, err := ps.GetAllPropertyIDs(ctx, take)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "could not get all properties")
// 	}

// 	props := make([]Property, len(propIDs))
// 	for i, propID := range propIDs {
// 		prop, err := ps.GetPropertyByID(ctx, propID)
// 		if err != nil {
// 			return nil, errors.Wrap(err, "could not get property by ID")
// 		}

// 		props[i] = *prop
// 	}

// 	return props, nil
// }

// LoadPropertyByFields loads a Property from the database using the fields of given Property.  This function is useful
// for loading Property records into memory when the caller is unsure if the Property reference it holds accurately
// represents a database record, or if it even exists.  It goes through the fields of the Property (in order of
// specificity) and searches on the first field that is non-empty.  If that field does not return a valid Property,
// this function returns an error
func LoadPropertyByFields(ctx context.Context, l plog.Logger, p *Property, ps PropertyStore, ts TagStore) (*Property, error) {
	id := p.ID
	var err error
	if id == "" {
		if p.Address != "" {
			if id, err = ps.GetPropertyIDByAddress(ctx, p.Address); err != nil {
				return nil, errors.Wrap(err, "could not get Property ID by address")
			}
		} else if p.URL != "" {
			if id, err = ps.GetPropertyIDByURL(ctx, p.URL); err != nil {
				return nil, errors.Wrap(err, "could not get Property ID by URL")
			}
		} else {
			// there are no fields provided that can be used to load
			// a Property and this function will return an error
			return nil, perr.NewErrInvalid("provided Property has no uniquely identifiable fields on it")
		}
	}

	loaded, err := ps.GetPropertyByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "could not load Property by ID")
	}

	tags, err := ts.GetTagsByPropertyID(ctx, loaded.ID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get tags")
	}

	loaded.Tags = tags
	return loaded, nil
}

// Save saves a property to the database
func (p *Property) Save(ctx context.Context, l plog.Logger, ps PropertyStore, ts TagStore) error {
	l.Info(ctx, "property to save", "property", *p)
	if p.URL == "" {
		return perr.NewErrInvalid("URL cannot be empty")
	}

	if p.Acreage < 1 {
		return perr.NewErrInvalid("properties must have at least one acre (rounded up)")
	}

	// Some lots don't have buildings on them
	// if p.SquareFeet < 1 {
	// 	return perr.NewErrInvalid("properties must have positive square footage")
	// }

	if p.Address == "" {
		return perr.NewErrInvalid("Address cannot be empty string")
	}

	if p.ID == "" {
		if err := p.insert(ctx, l, ps); err != nil {
			return errors.Wrap(err, "could not insert property")
		}
	} else {
		if err := p.update(ctx, ps); err != nil {
			return errors.Wrap(err, "could not update property")
		}
	}

	if p.Tags == nil {
		p.Tags = []string{}
	}

	if err := ts.SetTags(ctx, p.ID, p.Tags...); err != nil {
		return errors.Wrap(err, "could not set tags for property")
	}

	return nil
}

// GetCaptures retrieves all the already loaded Captures
func (p *Property) GetCaptures() []Capture {
	return p.captures
}

// LoadCaptures loads all captures into the Property receiever
func (p *Property) LoadCaptures(ctx context.Context, l plog.Logger, cs CaptureStore) error {
	caps, err := cs.GetAllCapturesByPropertyID(ctx, p.ID)
	if err != nil {
		return errors.Wrap(err, "could not get Captures")
	}

	p.captures = caps
	return nil
}

// helper methods

func deduplicatePropertyIDs(propIDs []string) []string {
	encountered := map[string]bool{}
	var result []string
	for _, propID := range propIDs {
		if encountered[propID] {
			continue
		}

		encountered[propID] = true
		result = append(result, propID)
	}

	return result
}

// getPropertiesByAddress gets all properties that match the address
func getPropertyIDsByAddress(ctx context.Context, l plog.Logger, address string, take int, ps PropertyStore) ([]string, error) {
	if address == "" {
		return []string{}, nil
	}

	propertyIDs, err := ps.SearchPropertyIDsByAddress(ctx, address, take)
	if err != nil {
		return nil, errors.Wrap(err, "could not get properties by address")
	}

	return propertyIDs, nil
}

// getPropertiesByTags returns all properties possessing the provided tag(s).
//
// Tags should be provided in the format: "<tag>,<tag>,<tag>" where each tag is separated by a comma
func getPropertyIDsByTags(ctx context.Context, l plog.Logger, tags []string, ts TagStore) ([]string, error) {
	if tags == nil || len(tags) < 1 {
		return []string{}, nil
	}

	propertyIDs, err := ts.GetPropertyIDsByTags(ctx, tags...)
	if err != nil {
		return nil, errors.Wrap(err, "could not load property IDs by tags")
	}

	return propertyIDs, nil
}

func (p *Property) insert(ctx context.Context, l plog.Logger, ps PropertyStore) error {
	_, err := ps.GetPropertyIDByURL(ctx, p.URL)
	if err != nil && !perr.SameType(err, perr.ErrNotFound) {
		return errors.Wrap(err, "could not get property ID from URL")
	} else if err == nil {
		return perr.NewErrInvalid(fmt.Sprintf("URL %s already exists in the database", p.URL))
	}

	_, err = ps.GetPropertyIDByAddress(ctx, p.Address)
	if err != nil && !perr.SameType(err, perr.ErrNotFound) {
		return errors.Wrap(err, "could not get property ID from address")
	} else if err == nil {
		return perr.NewErrInvalid(fmt.Sprintf("address %s already exists in the database", p.Address))
	}

	if err := ps.InsertProperty(ctx, p); err != nil {
		return errors.Wrap(err, "could not insert Property")
	}

	return nil
}

func (p *Property) update(ctx context.Context, ps PropertyStore) error {
	oldProp, err := ps.GetPropertyByID(ctx, p.ID)
	if err != nil {
		return errors.Wrap(err, "could not get old property by ID")
	}

	if oldProp.Acreage != p.Acreage {
		return perr.NewErrInvalid("acreage cannot be mutated without invalidating existing captures")
	}

	if oldProp.Address != p.Address {
		return perr.NewErrInvalid("address cannot be mutated without invalidating existing captures")
	}

	if err := ps.UpdateProperty(ctx, p); err != nil {
		return errors.Wrap(err, "could not update Property")
	}

	return nil
}
