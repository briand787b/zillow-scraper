package redis

import (
	"fmt"

	"zcrapr/core/perr"

	goredis "github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
)

// NewGoRedisClient instantiates a new Go-Redis Client
func NewGoRedisClient(host, port, password string) (*goredis.Client, error) {
	client := goredis.NewClient(&goredis.Options{
		Addr:     fmt.Sprintf("%s:%v", host, port),
		Password: password,
		DB:       0, // use default DB
	})

	if _, err := client.Ping().Result(); err != nil {
		return nil, errors.Wrap(perr.NewErrInternal(err), "could not ping Redis")
	}

	return client, nil
}
