package redis

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"zcrapr/core/model"
	"zcrapr/core/perr"
	"zcrapr/core/plog"

	"github.com/go-redis/redis/v7"
	goredis "github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
)

// PropertyRedisStore is a PropertyStore backed by Redis
type PropertyRedisStore struct {
	l      plog.Logger
	client *goredis.Client

	idCounterKey string
	idPrefix     string
	idWidth      string
	maxCursor    uint
}

// NewPropertyRedisStore returns a new PropertyStore backed by Redis
func NewPropertyRedisStore(l plog.Logger, idCounterKey, idPrefix string, c *goredis.Client,
	idWidth, maxCursor int) (*PropertyRedisStore, error) {

	if idCounterKey == "" {
		return nil, perr.NewErrInvalid("idCounterKey cannot be empty string")
	}

	if idPrefix == "" {
		return nil, perr.NewErrInvalid("idPrefix cannot be empty string")
	}

	if idWidth == 0 {
		return nil, perr.NewErrInvalid("idWidth cannot be zero")
	}

	return &PropertyRedisStore{
		l:            l,
		client:       c,
		idCounterKey: idCounterKey,
		idPrefix:     idPrefix,
		idWidth:      strconv.Itoa(int(idWidth)),
	}, nil
}

// GetAllPropertyIDs x
func (s *PropertyRedisStore) GetAllPropertyIDs(ctx context.Context, take int) ([]string, error) {
	var allKeys []string
	var nextKeys []string
	var err error
	var cursor uint64
	count := int64(take)
	for {
		nextKeys, cursor, err = s.client.Scan(cursor, s.idPrefix+":*", count).Result()
		if err != nil {
			return nil, perr.NewErrInternal(errors.Wrap(err, "could not execute Redis command"))
		}

		allKeys = append(allKeys, nextKeys...)
		if cursor == 0 {
			break
		}

		if len(allKeys) >= take {
			break
		}
	}

	return allKeys, nil
}

// GetPropertyByID x
func (s *PropertyRedisStore) GetPropertyByID(ctx context.Context, id string) (*model.Property, error) {
	fieldMap, err := s.client.HGetAll(id).Result()
	if err != nil {
		return nil, errors.Wrap(perr.NewErrInternal(err), "could not get all hash keys")
	}

	if len(fieldMap) < 1 {
		return nil, perr.NewErrNotFound(errors.Errorf("could not find Property with ID %s", id))
	}

	s.l.Info(ctx, "returned redis hash map", "hashmap", fieldMap)

	id, ok := fieldMap["id"]
	if !ok || id == "" {
		return nil, perr.NewErrInternal(errors.New("returned ID is empty"))
	}

	url, ok := fieldMap["url"]
	if !ok || url == "" {
		return nil, perr.NewErrInternal(errors.New("returned url is empty"))
	}

	acreageStr, ok := fieldMap["acreage"]
	if !ok || acreageStr == "" {
		return nil, perr.NewErrInternal(errors.New("returned acreage is empty"))
	}

	acreage, err := strconv.ParseFloat(acreageStr, strconv.IntSize)
	if err != nil {
		return nil, errors.Wrap(perr.NewErrInternal(err), "could not convert acreage to float64")
	}

	var sqft int
	sqFtStr, ok := fieldMap["sqft"]
	if !ok || sqFtStr == "" {
		s.l.Info(ctx, "could not load sqft attribute",
			"id", id,
			"field_map", fieldMap,
		)
	} else {
		sqft, err = strconv.Atoi(sqFtStr)
		if err != nil {
			return nil, errors.Wrap(perr.NewErrInternal(err), "could not convert sqft to int")
		}
	}

	address, ok := fieldMap["address"]
	if !ok || address == "" {
		return nil, perr.NewErrInternal(errors.New("returned address is empty"))
	}

	return &model.Property{
		ID:         id,
		URL:        url,
		Acreage:    acreage,
		SquareFeet: sqft,
		Address:    address,
	}, nil
}

// GetPropertyIDByAddress x
func (s *PropertyRedisStore) GetPropertyIDByAddress(ctx context.Context, address string) (string, error) {
	propertyIDs, err := s.findPropertyIDsByAdddress(ctx, address, 2)
	if err != nil {
		return "", errors.Wrap(err, "failed to find property IDs by address")
	}

	if len(propertyIDs) != 1 {
		return "", perr.NewErrNotFound(errors.Errorf("could not find property ID by address (%s)", address))
	}

	return propertyIDs[0], nil
}

// GetPropertyIDByURL x
func (s *PropertyRedisStore) GetPropertyIDByURL(ctx context.Context, url string) (string, error) {
	id, err := s.client.Get(url).Result()
	if err != nil {
		if err == redis.Nil {
			return "", perr.NewErrNotFound(errors.New("url does not exist in database"))
		}

		return "", errors.Wrap(perr.NewErrInternal(err), "could not get id by url")
	}

	return id, nil
}

// InsertProperty x
func (s *PropertyRedisStore) InsertProperty(ctx context.Context, p *model.Property) error {
	currMaxID, err := s.client.Incr(s.idCounterKey).Result()
	if err != nil {
		return errors.Wrap(perr.NewErrInternal(err), "could not increment ID counter")
	}

	base16ID := s.getPropertyIDKey(currMaxID)

	txPipe := s.client.TxPipeline()
	defer txPipe.Close()
	txPipe.HSet(base16ID, map[string]interface{}{
		"id":      base16ID,
		"url":     p.URL,
		"acreage": p.Acreage,
		"address": strings.ToLower(p.Address),
	})
	txPipe.Set(p.URL, base16ID, 0)
	txPipe.SAdd("adrs", s.createAddressSetElem(p.Address, base16ID))
	if _, err := txPipe.ExecContext(ctx); err != nil {
		return errors.Wrap(perr.NewErrInternal(err), "could not execute transaction")
	}

	p.ID = base16ID

	return nil
}

// SearchPropertyIDsByAddress x
func (s *PropertyRedisStore) SearchPropertyIDsByAddress(ctx context.Context, address string, take int) ([]string, error) {
	searchTerm := "*" + strings.ToLower(address) + "*"
	return s.findPropertyIDsByAdddress(ctx, searchTerm, take)
}

// UpdateProperty x
func (s *PropertyRedisStore) UpdateProperty(ctx context.Context, p *model.Property) error {
	if err := s.client.HSet(p.ID, map[string]interface{}{
		"url": p.URL,
	}).Err(); err != nil {
		return errors.Wrap(perr.NewErrInternal(err), "could not execute redis command")
	}

	return nil
}

// ////// HELPER METHODS //////

func (s *PropertyRedisStore) findPropertyIDsByAdddress(ctx context.Context, keyword string, take int) ([]string, error) {
	var ids []string
	var nextKeys []string
	var err error
	var cursor uint64
	for {
		nextKeys, cursor, err = s.client.SScan(s.getAddressKey(), cursor, keyword, int64(take)).Result()
		if err != nil {
			return nil, perr.NewErrInternal(errors.Wrap(err, fmt.Sprintf("could not execute Redis SCAN %s", keyword)))
		}

		s.l.Info(ctx, "iteration of address key scan",
			"nextKeys", nextKeys,
			"cursor", cursor,
		)

		nextIDs := make([]string, len(nextKeys))
		for i, key := range nextKeys {
			keyElems := strings.Split(nextKeys[i], ":")
			if len(keyElems) != 3 {
				return nil, perr.NewErrInternal(errors.Errorf("addresses set element (%s) has invalid format: key does not have 2 colons", key))
			}

			nextIDs[i] = keyElems[1] + ":" + keyElems[2]
		}

		ids = append(ids, nextIDs...)
		if cursor == 0 {
			break
		}

		if len(ids) >= take {
			break
		}
	}

	s.l.Info(ctx, "final ids found by searching addresses",
		"ids", ids,
	)

	return ids, nil
}

func (s *PropertyRedisStore) getAddressKey() string {
	return "adrs"
}

func (s *PropertyRedisStore) createAddressSetElem(address, id string) string {
	return address + ":" + id
}

func (s *PropertyRedisStore) getPropertyIDKey(currentMaxID int64) string {
	return fmt.Sprintf("%s:%0"+s.idWidth+"x", s.idPrefix, currentMaxID)
}

// func (s *PropertyRedisStore) removeIDs(keyList []string) []string {
// 	var stripped = make([]string, 0, len(keyList))
// 	for _, key := range keyList {
// 		if !strings.Contains(key, "id:") {
// 			stripped = append(stripped, key)
// 		}
// 	}

// 	return stripped
// }

// //  internal keys are keys that the db uses internally and are not meant to be exposed
// func (s *PropertyRedisStore) removeInternalKeys(keyList []string) []string {
// 	internalKeys := []string{s.idCounterKey}

// 	var stripped = make([]string, 0, len(keyList))
// 	for _, key := range keyList {
// 		for _, internalKey := range internalKeys {
// 			if !(key == internalKey) {
// 				stripped = append(stripped, key)
// 			}
// 		}
// 	}

// 	return stripped
// }

// func (s *PropertyRedisStore) removeURLs(keyList []string) []string {
// 	var stripped = make([]string, 0, len(keyList))
// 	for _, key := range keyList {
// 		if !strings.Contains(key, "http") {
// 			stripped = append(stripped, key)
// 		}
// 	}

// 	return stripped
// }
