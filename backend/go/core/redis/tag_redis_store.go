package redis

import (
	"context"
	"zcrapr/core/perr"
	"zcrapr/core/plog"

	goredis "github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
)

// TagRedisStore is a TagStore backed by Redis
type TagRedisStore struct {
	l      plog.Logger
	client *goredis.Client
}

// NewTagRedisStore instantiates a new TagRedisStore
func NewTagRedisStore(l plog.Logger, c *goredis.Client) *TagRedisStore {
	return &TagRedisStore{
		l:      l,
		client: c,
	}
}

func (s *TagRedisStore) GetTagsByPropertyID(ctx context.Context, propertyID string) ([]string, error) {
	tags, err := s.client.SMembers(s.getPropertyTagsKey(propertyID)).Result()
	if err != nil {
		return nil, errors.Wrap(err, "could not execute SMEMBERS command")
	}

	return tags, nil
}

func (s *TagRedisStore) SetTags(ctx context.Context, propertyID string, tags ...string) error {
	s.l.Info(ctx, "property and tags to save",
		"propertyID", propertyID,
		"tags", tags,
	)

	txPipe := s.client.TxPipeline()
	defer txPipe.Close()

	if _, err := s.client.Del(s.getPropertyTagsKey(propertyID)).Result(); err != nil {
		return errors.Wrap(err, "could not DEL key-tag association")
	}

	for _, tag := range tags {
		if _, err := txPipe.SAdd(s.getTagsKey(), tag).Result(); err != nil {
			return errors.Wrap(err, "could not SADD tag")
		}
		if _, err := txPipe.SAdd(s.getPropertyTagsKey(propertyID), tag).Result(); err != nil {
			return errors.Wrap(err, "could not SADD property tag")
		}
		if _, err := txPipe.SAdd(s.getTagPropertyIDsKey(tag), propertyID).Result(); err != nil {
			return errors.Wrap(err, "could not SADD property ids")
		}
	}

	if _, err := txPipe.ExecContext(ctx); err != nil {
		return errors.Wrap(perr.NewErrInternal(err), "could not execute transaction")
	}

	return nil
}

func (s *TagRedisStore) GetAllTags(ctx context.Context, keyword string, take int) ([]string, error) {
	var cursor uint64 = 0
	var tags, resultKeys []string
	var err error
	for {
		if resultKeys, cursor, err = s.client.SScan(s.getTagsKey(), cursor, "*"+keyword+"*", 30).Result(); err != nil {
			return nil, errors.Wrap(err, "could not execute SSCAN")
		}

		tags = append(tags, resultKeys...)
		if cursor == 0 {
			break
		}

		if len(tags) >= take {
			break
		}
	}

	return tags, nil
}

// GetPropertyIDsByMultipleTags returns all propertyIDs that are common to every set pointed to by the provided tags
func (s *TagRedisStore) GetPropertyIDsByTags(ctx context.Context, tags ...string) ([]string, error) {
	keys := make([]string, len(tags))
	for i, tag := range tags {
		keys[i] = s.getTagPropertyIDsKey(tag)
	}

	tags, err := s.client.SInter(keys...).Result()
	if err != nil {
		return nil, errors.Wrap(err, "could not execute SINTER")
	}

	return tags, nil
}

func (s *TagRedisStore) getPropertyTagsKey(propertyID string) string {
	return "tags:" + propertyID
}

func (s *TagRedisStore) getTagsKey() string {
	return "tags"
}

func (s *TagRedisStore) getTagPropertyIDsKey(tag string) string {
	return "tag:" + tag + ":ids"
}
